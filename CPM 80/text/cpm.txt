��� ��� ����: ������������ ������� CP/�.


1. ������� ��������.

    � 1972 ���� ������� ������� �� ����� ���� ������� ���������� ����������� � ����������� ������-�������� �������� � ��������, ����������. ������� �� ������ �� ����� ���������� ������� ������� ��������������� �� Intel � �������������� 4004. ���� ����� ���������� ��� ���� ���� �������������� � ����� ������ ��� ���� ��������� ���������. ������ �� ������� �������� Intel � ���� ������� ������������� ������������� � ����� �� �� ��������� �������������. ������� ���� ���� � ������ �� Intel � ��������� �� ����� �����, ���� ������� ������ ���� ���������������� ��� ��������������� Intel 8008. �� ������ ���� ���� PL/M - Programming Language for Microcomputers (���� ���������������� ��� ����������������). � �������� ����� ������ Intel �������� �������� Intellec-8 � ������� ����������, ������� ���� ���������� ��������������� Intel ��� ������ � ����������� 8080, ��������� ��������� � ����������� ��� ������ ���������. � 1973 ���� ���� ������� ������� ������ �������� ����� Shugart. ���� �������� John Torode �� ��� ������� ��������� ���������� ��� ����� ����������.
����� ������� ������� ���������� �������� � ��� Intellec-8, ��� ���������� ��� ������� � ��������� ��������������. ������ ����� ������ �������� ������������ ����� ������������, �� ���� ���������� ����������� �����������, ������� ��������� �� ������� �������� � ����������. � ���� ������� ������ ����� ��. ��������� ���� ���� PL/M �� ������� ������ ������������ ������� ��� ����������������. �� ������ �� CP/M (Control Program for Microcomputers).
    ���� �������� ������ CP/� � 1974 ����, � ����� �� ��������� ���������� �� Intel �� 20.000 ��������. �� Intel �������� ��� �� �������������� � ������������ �������� ������������ �������. ����, ����� ��������, ���� PL/� Intel ������.
    � 1976 ���� ���� � ��� ������� Dorothy McEwen ������� ����� ��� ��������� �Intergalactic Digital Research� (������� ����� ���� ��������� �� Digital Reserach Inc ��� DRI) ��� ������ CP/M. ���� ��������� �����������������, � ������ ��������������� ������ �����. � �������� 70-� ����� ���������������� ����� �����, � CP/M ��������� � ������ ����� � ������ �����. ����������� �������������� ����������� �� ������ �������������� ������������� ������������ �������, � ��� ������������� ������������� ����������� ��� ������� ��������, ����� �� ������� ���� CP/M.
    �� ���������� 70-� Digital Research ��������� 2 ������������ ������ CP/M. ������ ������ CP/�. 1.4 �������������� �� ��� ������, �� ������ ������ CP/�. 2.2, ����� ������������ ����������. ���� �� ��������� ��������� � 70-�� �� ���� ���������� Intel 8080 ��� Zilog Z80, �� ������������ � ������������ �������� CP/M. � ����� 70-� ������ Digital Research ���� ������� ����� �������� �������� ����� ������������ �������, ������� �������� ����� ��� �� 3000 ����� ����������������.

2. ������������� CP/M.

    ������������ � ����� ���������������� ������������ �����, ��� �������������� ��� ������� ��������� ��������� �����. ���, � ���� �������, �������� ���������� � ����������� ���������� � � ������ ������� � ������������ �������.
��������� �� ��/� �������� ������������ �������� ������������� ��� ������ ������������ � ����� ������������ ��������� ����������� ������ ����� ���������, Digital Research ����������� ��������� �������������� CP/M, ������� ��������� �������� ������������� ��� ������ ������������.
��� �� ������� ��������:
-  MP/M , MP/M-II (Multiprogramming Monitor for Microcomputers)
��� ���������������������, ������������� ������������ ������� ��� �����-��� ��������� �� ���� ���������������� Intel 8080, Intel 8085 � Zilog Z80. ������������ ������� MP/M-II �������� ������ ������� MP/M �� ����������� ������� ��������� ����������� ������� � ������ ������������ �������������. ���������� � ������������ �� ��, ��� � � CP/M, ������ � ���������� ���������, � ������:
> ������� ��� ������� 48 �� ����������� ������
> ������� ���������� �� �������
������ ����� � MP/M-II ������� �������� ���������� ������������ ������������ �� 16 ���������� ��������� � 16 ��������� �����-������.
- CP/�-86
��� ������������ ������� ����������� �� ���� CP/M � ������������� ��� ���������������� �� ����������������, ��������� �� ���� ����������� Intel 8086  � Intel 8088. CP/M-86 � �������� ���������� CP/M, ��������� ��� �������� ������������ �������� ������ ������������, ������������ � �������� �� �� ������� � ���������� ��� �� �������� ���������� �������������� � �������������. ������ �� CP/M-86 ������������� ��� ������ � ����������������� ������� ���� � ������� �������� ����������� �������� ������������� �� �������������� � �����������.
- Concurrent CP/M-86
������������ ������� Concurrent CP/M-86 ������� �� ���� �� CP/M-86 � �������� ������ ����������� ����������� OC CP/M ������ 3.0. ��� ������������ ������� ���������� �� �� CP/M-86 ���, ��� ��� ������������ ������������ ����� ���������, �.�. ��������� ������������ ��������� ��������� �������� ������������. ��� ����������� �� ���� ������������� ��� ���������� ������������ �������. ����������� ������� ��������� ����� ������������ �������� ����� ����, ��� ������������ �������� ���������. � ����������� �������� ���������� ��� � �������� ���������� �����������, ������ ���������� �� ��������� �������������� ������������ ��������.
- MP/M-86
������������ ������� MP/M-86 �������� ���������������������, ������������� ������������ ��������, ��������������� ��� ���������������� �� �����-���, ��������� �� ������ ��������������� Intel 8086 ��� Intel 8088. � ����� MP/M-86 ����������� ��� ���������� � �� CP/M-86 � �������� � �������� ���� �� ���������������� ��� � MP/M-II.
- CP/NET
������������ ������� CP/NET ������������� ��������, � ������� ������� ��������� ����������������, ���������� �� CP/M, OC MP/M ��� MP/M-II ����� ���������� � ���� � ���, ����� ����� ����������� ����������� ������������� �������� ������ �������. � ����� ��������� ������������ �������� ��������� �������� �����, ���������� ������, ���������� ��������� � ��������� ��������� ��� ������. ���� ������ ������� �� ������� �������������� �������, ���������� OC MP/M ��� �� MP/M-II � ��������������� ������, ���������� �� CP/M. ������� �������������� ������� ������������� � �������� ������� ���, ����������� ������������ ���������, � ������� ����� ���������� ��������������� �������, ���������� � ����. � ������� �� CP/NET ����� ��������� �������������� ���� ���������� ����, � ��������� ��������������� ������ ����� ��������� � ��������� ������� ��������. �� CP/NET ����� ��������� ���������� ��������� ����� ������� � ���������������� ���������, � ��� ����������� ����������� ����� ������������ ��������� ��������������� ����� ����� ������� �������� � ����������������. 
    ������������ ��������� ������ ������������ ������� CP/M �������������� ��� ������ �� ���� ��������� ����������������: CP/M-68k ��� Motorola 68000, CP/M-8000 (CP/M-8k) ��� ���������� Zilog Z-8000 � ��.
    ������� ��������, ��� � ��������� ����� CP/M ������ 2.2 ���� ������������� ���������� �� ������� ����. ������ ������� �������� �������� �����-��� � ���� ������������ ��� ������ �� ��������� ����������� ���� �������, ������� � �robotron�. 

3. �����������, �������� �������.

     ����������� ��������� ���������� ��� ������� 8-������ ������ CP/M ���������:
- �������������� Intel 8080, Zilog Z80 ��� ������
- ������� 16 �� ���
- ������� 1 �������� ����������
- �������� 
�������������� ��������� ������������ ������� ������ ��������� � BIOS ��� �� �� �����(�������). ����� �� ���������  ���������� ����� ������������ �������. ������������ ������� CP/M, �������� � ������, ��������� ������� �� 4 �������� �����:

- BIOS (basic input/output system)
- BDOS (basic disk operation system)
- CCP (console command processor)
- TPA (transient program area)

    BIOS ������������ ������� �������� �����-������: ������ � ������, � ����� � ������������� ������������. BIOS �������� ���������-��������� ������. ��� ������ ������ (������������) ��������������� ��������� ���� ����.
    BDOS ������������ ���������� �������� ��������, ����������� ���� ��� ����� �������� ����������� � ������������ ��������� ���������. BDOS ��������� ��������� ��������� ����������� �������� ��������:
- ����� (���� ���� �� �����)
- �������� (��������� ���� ��� ���������� �������� � ���)
- �������� (��������� ����)
- �������������� (�������� ��� �����)
- ������ (��������� ������ �� ��������� �����)
- ������ (���������� ������ � �������� ����)
- ����� (�������� ���������� ����)
    CCP �������� ��������������� ��������� ������. ����������� ������� ������������ � ������������ ������� CCP  ��������� � ��������� �������, ������� ������������ ������ � ��������� ������
    TPA � ��� ������� ������, � ������� ������������ ������� ������ ����������� ��������� � �� ������. � �������, ���� ������������ �������� ��������� �������� ED, �� ��� ��������, � ����� ����, � ������� �� ��������, �� ����� ������ �������� � TPA.

�������� �������.
��� ����� �� ����� ������� �� ��������� ������: ��� �����, �� 8 ��������, ����� ������� �����, ����� ���������� �����, �� 3 ��������.
������: <STAT.CMD>
������������ ������� � ����� �����: < > . , ; : = ? * [ ] % | ( ) / \
���������� ������ ��������� �� ��� �����, �� �� ����� ������ ������ �� ��������.
������ ����� ����������� ����������� 128-�������� �������, ������� ��������������� ��������������� �������� �� 8� ��������. ������� �� ���� ������� ������� ������ ������� ������ ���� �� ����� �������� ����.
������� ����������� ����� (timestamps) ���������� �� ��������������, ���� ��������� ����� ������� �������� CP/�. �������� ��� �����������.
�� CP/�. 2.2 �� ������������ ������� ������������ � ��������� �����, �� ������������ 16 "���������������� ��������", ����� ������������ �������� ������ �� �����. 
������� ���������������� �������� ���������� �������������������� �� CP/M � ��������������������� MP/M.
������� �USER ����� (c�.����) ����������� ������������� �� 0 �� 15. ������������ � ������� 0 ��� �� ���������. ���� ������� �� ����������� ����� ������������ � ������ 0 �� 1, �� ���������, ����������� ��� ������������ 0, ����� ���������� ������������ 1.

4.  ������� CP/�.

    � ������������ ������� ��/� ���������� ��� ���� ������: ���������� ������� � ���������� �������. ���������� ������� �������� ������ ������������ ������� � ��������� �������� ������ � ��� � ����� � ��� �� ������� ������. ���������� ��������� ������ � ��� ��, ������� ������ �������� �� ����� � ���� ��������� (.COM ��� CP/M ��� .CMD ��� CP/M-86) ������, �. �. ��� ������������� �����-���� ���������� ������� ���������� ������� ���������������� ����� � ���������� ��������.
    ���������� �������. ������������ ������� ��/� �������� ������, ������� ���������� "��������� ������ �������" (���). ������ ��� �������� ������������� �� ��/�, ������� ��������� ��������� ���������� ������� �� ������������ ��������� �������� �������� �� ��/�. ������ ��� �������� � ���� ����� ���������� ��� "�����������" ������, ��� ��������� � ������� �� ��������� ������������� �����. ����� ���� ���������� ������ �������������� � ������� ����� � �������� ������ �� ������ �� ��/� (� >, � > � �. �.) ����� �������. ��������� ��� ������� �� ���������� � ��������� ������, ��� �������� ��� ����������� ���������� �� ����, ����� �������� ���������� �������� �������, �� ����, � ������� �� ���������� ��������, ��� ���������� ���������� ������ ��������� � ����� �� ���������. ���� ������������ ������� �������� ������ ���������� ������� �� ��/�.
 
- DIR
������� DIR ��������, ��-��������, �������� ����� ������������ �������� �� ��/�. �������� DIR �������� ����������� ����������� ����� directory (�������). ������� DIR ������������ ��� ����������� ��� ������ ���������� ������ ���� ������, ������������ �� ��������� �����
-REN 
�������� REN �������� ����������� ����������� ����� rename (�������������). ������� REN ������������ ��� ��������� ����� ������, ���������� �� ������������ �����.
- ERA
�������� ERA �������� ����������� ����������� ����� erase (�������). ������� ERA ������������ ��� �������� ������, ���������� �� ������������ �����. 
- TYPE.
 ������� TYPE ������������ ��� ��������� �� ������� ����������� ������.
- USER
 ������� USER ������������� ��������� ������������� �� ��/� �������� ��������� � ����� ������, ���������� �� ����� � ��� �� �����. � �� ��/� � ����� � ��� �� ������ ����� �������� �� 15 �������������, ������ ������� ������� ����� ������� ������������ (������������ ��� ������ ������) �� ������������. ������� USER ������� � ������������ ������ � ������ 2.0 � ����� ������� ������� �� ��/� 
- SAVE
������� SAVE ������������ ��� ����, ����� ����� ���������� ���������� ����� ������� �� ��������� ������� ����������� ������ � ��������� ��� � ����� �� ������������ �����. ��� ������� ������ ������������ ������ ���������� ��������������.

    ���������� �������. ���������� ������� (�������) ���������� �� ���������� ������ ���, ��� ��� ��������, ���� �� ����� �� ����������, �������� � ����������� ������. ������� ����������, ����� ���������� ��������� ����������� � ������, ������������� �� �������� ���������. ��� ���� ����� ������������ ���������� ��������, �� ������� ������� �� �����, ������������� � ����� �� ��������� �������. 
����� ������� ������ �� �������, ������� ������ ��������� �� ��������� ����� �� ��/�. ������� ��������, ��� �� ��������� ����� ����� ���������� � ������, ������������� ���������-������� (������ Digital Research �� ������������). 
���� ������������ ������� �������� ����������� ���������� ������. ��� ������� ������� �� ���� ������� (1.3, 1.4, 2.0 � 2.2) �� ��/�, ���� �� ��������� �����.
- STAT
�������� STAT �������� Status ��� Statistics � ��������� ��� ����������. ������� STAT ������������� ������������ �� ��/� ��������� �������� ��� ���������� ������� � ����������� ������� ������, ��������� �������� � ������ �����������. ������� ������ ������������ ��� ��������� �������� �����, ������ ��������������� ��������� ������������ (� ����������), ��������� ��������� ��������� �����. 
 - PIP
�������� PIP �������� ����������� �� ����������� Peripheral Interchange Program (��������� ������ ������� ����� ������������� ������������). ������� PIP ������������ ��� ����������� ������ � ������-���� ���������� �� ���������� ���� �� ��� ������� ����, �������� � ����� �� ����, � ����� �� ���������� ����������, � ������� �� ����, � ����� �� ����� � �. �.
- ED
�������� ED �������� ����������� ����������� ����� EDitor (��������). ������� ED ��������� ������������ �� ��/� ��������� ��������� ��������, ������� ����� ��������� � ������������� ��������� �����.
- SUBMIT
 ������� SUBMIT ������������� ����������� ��������������� ������� ��������� ������������������ ������, �������������� �������������� ������������� � ��������� �����.
- XSUB (������� ������ � ������ 2.0 � ����� �������).
 �������� XSUB �������� ����������� �� ����������� Extended SUBMIT (����������� ��������� SUBMIT). ��������� XSUB ������ � ������ �������� ������� SUBMIT. ��� ��������� ������� ������ �� ������� ���������, ��� ���������� �� ���������� ���������� SUBMIT.
- ASM
�������� ASM �������� ����������� ����������� ����� Assembler. ��������� ASM "������������" ����� � ���������� �� ����������, ��������� �� ������� ED ��� �� ������� ������������ ���������� ���������, � ��� ���������� ".HEX" ����� (�����������������), �������, � ���� �������, ����� �� ������� LOAD ������������� � ����� � ����� �������� ��� ��������� �����. ��������� ASM ������������ �������������� �� ���������� ��� �������� ����������� ���������� ��������.
- LOAD
������� LOAD ������������ ��� �������������� ������������������ (".HEX") ����� � ����, ���������� �������� ��� ��������� ��� ���������.
- DDT
�������� DDT �������� Dynamic Debugging Tool (���������������� �������� ��� ������������ ������� ��������). ��� � �������, ������� ������������� ������������ �� ��/� ����� ����������� � ���������� ������ � ����������, �������������� � ��������   ��������� ���� ��� ����������������� �������, � ����� �������� ������ ������� ������� �����-������ (BIOS) � ������� �������� ������������ ������� (BDOS).
- DUMP
������� DUMP ������������ ��� ������ � ����������������� ������� ����������� ����� � ���������/�������� ����� �� ����� ������� ��� ���������� ����������.
- FORMAT
������� FORMAT ������ ��� ���������� ����� � ������ ����������. ��� ������������� ��������� �������������� ������ ���� � ������ ����������� ����������� � �� ��/� ������. ����� ����, ������������� ������� ����� �������� � �������, ���������� ������� ���� ���� "���������" (������� ����). ��������� FORMAT �������� �� ����������� � ����� ����� �������������� �������� � ������ � ����������� �� ���������� ������ ������������ ������� ��/�. �� ��������� ��� ���������� � �������� �������� ��������� FORMAT �������� �������� �����������, � �� ����� ��� ������ ���������� ����������� ������� � �������� �������� �� ������� � �������. ����� �������, ����������� �������� �������� ���������� �� ������������ ����������� ���������������.
- SYSGEN
�������� SYSGEN �������� ����������� �� ����������� System Generation (��������� �������). ������� SYSGEN ������������� ������������ �������� ��� ������������� ������������ ������� ��/� ��� ����������� �� � ������ ����� �� ������.
- MOVCPM
�������� MOVCPM �������� ����������� �� ����������� "Move ��/�" (����������� ��/�). ������� MOVCPM ��������� ��� ����������� ������������ ������� ��/� � ���, ����� ��� ���� ������������ ��� ��������� ������ ���, ���� ��������� ����� ������ ��� ����������� ��������-����������� ��������� �������������.

    ������� ����������� ��������. �� ��/� ������������� ������������ ��������� ����������� ������, ��� ���������� "������� ����������� ��������". ��� ������� ���� ������������ �� ��/� ����������� ��������� � �������� ������ ����� ��������� ����� � ����������� ����������� �� ���. ��� ������� ���������� "������� ����������� ��������", ��������� ��� �� ���������� ���������� ������������ ������ �� ���������� ������� "CONTROL" (Ctrl) � �������, ��������������� ���������� �������. ���� �������� ������ ������ ������:
CTRL/C  - ��������������� �������
CTRL/M � ������� �������
CTRL/J � ������� ������
CTRL/H � ������� ������� ����� �� ���� �������
CTRL/U � ������� ������� ��������� ������
CTRL/X - ������� � ������� ������� ��������� ������
CTRL/E � ������� ������� ��������� ������ �� ��������� ������
CTRL/R � ������������ ����������������� ��������� ������ �� ��������� ������
CTRL/S � ����������/���������� ����������� ������ �� ������ �������
CTRL/P � ������� �� ������ ����� �����, ��������������� �� �������


5. ������� ������, ��� ������� � IBM � Microsoft.

    � 1980 ���� ����� IBM �������� ��� ��������� ������ ������� ������������� ���������� � IBM PC. ���������� ������������ ���� ����������, �� IBM ��� ��� �� ������� ����������� ����� - ���� ���������� ������������ ������� � ���� ����������������. ����������� ������������ ����������� ���������� ������� �� �� ����� ����� Microsoft. ����� ���������� ���� ������ ��������� � ���������, � ������ ���� ����� ��� �������� �������� IBM � ���, ��� ������� � ������ ���������������� ���, �� ������������ ������� Microsoft ������� �� ����� �������� ����� �� ������. ������ �������� ������� ������������� ���������� �������� ��� �������, �.�. ��� ������������ ������� ��� ���� ������ ���� ������.
����� ���� �������, ��� ���� ������ ���� �����, ��������� ����������� ������������ ������� ��� ���� IBM � ��� Digital Research. � ���� ��������� �������� ��������������, ����� � �������� Digital Research ���� ������� ������ �������� ������ ������� ��������� ����������� �����������. ����� ��������� IBM  ���� ����� ��� �� �������� �������� � �������� ������� �� ��������� ����. 
    22 ������� 1980 ���� ������������� IBM ������� �� ������� � ������ Digital Research. �� � ���� ����������� ������� �� �������� ����������. �������� ����� ��, ��� ���� ������� ��������� ����������� ������������� ������� � ���������� �����������. ����� �� IBM ���� � ������� ������
���������� ������� � ���, ��� ����, �����, ���� �� ���� ������������� � IBM. � ��� ����� �� ����� ��� ������� �� ����� ������ ��������, ��� ��� ������ �������� �������. ������ ���� �� ������� ���������� ����� �������, ��� ������������ ��������� ����� � ������ IBM �� ��� ������������ ������� CP/M.
    � ���� ������� ������ Microsoft ���� ����������� � Seattle Computer �������� �� �������� ����� � �������������� ������������ ������� 86-DOS (��� ��� �� ��� �������� Q-DOS). ������, 86-DOS ������������ ����� �� ��� ���� ��� �� CP/M, �������������� (� �������� ����) ������������ ��� ������������� ��� Intel 8086 ����� ������������� �� Seattle Computer. ������ ������������ ����� �������, ��� ��� �� ����������� ����� �������� ��� ����������� ����������� ��� CP/M
����� ��������� ���� ����� ��������� �� IBM ��� ������������� � ������ ������������ ����������. ��� ��������� MS-DOS� 
    ������������ ������� �� Microsoft �� ����� ������ �������������� ��������� �� IBM PC. ������ Digital Research ��������� 16-��������� ������ ��/�-86. � IBM ����������� ��������� �� ������ �� ����� ������������ �����������. �� ������ ����� ����� ����� �� � ������������ ������ �������� �������. ��� ����������� ������� ������������ � ������������� ��/� ������ ����� ������������ �� �����, �� ������������ ������. ������ ��/� ���� ����� ��� � 6 ��� ������ MS-DOS, ����� ����, IBM ������� ������� ��������� ���������� Microsoft, ������������ �������, ������� ������������ ������ � IBM PC, � PC DOS. � ���� �� ����� ������ ����� ������ (��� �������� ����������� �������) ������� ��������� MS-DOS �������������� ����������� ��� ����, ����� ��� ���������� �� ������ �� ����� �������������. ������� ��, ��������, �������� ���� ������� ������ �������� ������������� (�� ������� ����, �������). ��� �� �����, Microsoft ������������ ����� ���� ���, ����� �������� ������������� ������ ��� �����������.
� ���� �������, ����� IBM � 1983 ���� ��������� PC/AT �� ������ ���������� Intel 80286, MS-DOS ��� ������ ���������� �������, � CP/M ������ ������ ������������.

6. ���������� ������ CP/M

    ����� ������ ������ � IBM ���� � ������ ������, ��� ��������� DRI ����� ����� ���������������. ���� ������ �� ���������� Digital Research � �������������� �� ��������� ����������������� � ������-����������������� ��������.
����� �� ��� ��� ���� ���������������� LOGO. ������� ��������, ��� LOGO ��� ��������������� ������� ����� LISP, �������� Microsoft BASIC �� ��������������� �����. �� ����� �� ���������. ��������� �������� ���� �������� ����� ���������� ������������ ����������������� ������������ ���������� ��� CP/M, ������� ������� �������� Graphical Environment Manager (GEM) Desktop. ����������� ���� ������� ��� ������ �������� CP/M. ��� ��������� FLEX OS, Concurrent DOS/386, DOS-Plus. �� � ��� ��� �� ����� ������� CP/M ����� ������������. ����� ���� ������� 
    Digital Research ���� ������� ��������� Novell � 1991 ����. ��� ������ �������� ���� ��������. ����� �� ���������������� ������������� Digital Research ������� � �������� Novell, ������� �� ������ CP/M ������������� ���� ������ DOS�� DR-DOS (multi-user DR-DOS). 
    � 1996 ���� Novell ������� ����� �� DR-DOS �������� Caldera, ������� ����������� ������������ ������� DR-DOS (���������������� ������ CP/M) � ����������� ����������� ����������� � ��������� �� ��� ������ ������ -  Caldera OpenDOS.
    � 2001 ���� �� ���� ������������� �������� Caldera ���� ������� �������� Lineo, ������� ������������ ��� ����� �� CP/M � DR-DOS.
19 ������� 2001 �������� Lineo ��������� ��������� ����� ��������� �� CP/M ���� ����� ����������� � ���, ��� ��������� � ���������� ����� �������� ����� �� ��������������� � ��������� ��������� ���� CP/M � �������������� �����. 
    ����� �������, CP/M ������ �������� ���������� open-source, ��� ��� ������ ����� �������� � �������������� ������� �������� ����������� ���������. ������ ������������ ���������� �GEM� ����� ����� ������ open-source. ��� ���������� ���������� ����������� ����������. GUI ������� ��������� OpenGEM � �������� �� ������: http://gem.shaneland.co.uk
    ������� ��������, ��� ����� ������ � ����������� ��, ����������� �� ���� CP/M �� ����������� ���� �������� OC REAL/32. ������ ��������� ����� ����� ������� DRI  Novell �������� ����� �� �������� � ��������������� ������������ ������� ����� IMS (Intelligent Micro Software,��������������). ������� � ������ 7.51, ���������� � ������ 1995 ����, �� ���������������� ��� ������ REAL/32. ������ REAL/32 ���: 
 - 32-������ ���� ��������� �������
- ������������� ��������������������� ��
- ��������� �� 64 �������������, �� 82 ���������������� � 3 ������������ ������ ��� ���������� ����������, ��������� �/��� �������������� ���������
- �� 8 �������� (DOS / WINDOWS ������), ���������� ������������ � ������� ������������ 
- ���� � Novell NetWare ��� ��������� �� 16 ������������� NetWare ������
� ������ ������
    

7. ����� �������.
    ��������� ���������� �������� ������� CP/M � MS-DOS ���� ��������������, �� ���� ���������� ����������� �����������, ������� ��������� �� ������������ �������  ����� ����� ������������� ���������, ��������� �������� ����������.
�� ���������� ��� ���������:
>22disk  (http://ftp.gaby.de/pub/dos/22dsk144.zip)
������ ������� ����������� ������ Sydex. ��������� ��������� ������ � 1.44, ������������ 31 ������� 1996 ����. ��������� ����������� ��� �� MS-DOS � ��������� ��������� ��������� ��������: ����� ������� ����� MS-DOS � CP/M ����� �������, � ����� �������� ������ CP/M � �������������� ������� � ������� CP/M.  
�������� ���� � CMENU.EXE. ����� ��� ������� �� ������ ���������� ����:
0 � ����� � ���
1 � ������� ��� ������� CP/M
2 �������� ���������� ���� CP/M
3 � ���������� ����� CP/M � MS-DOS
4 � ���������� ����� MS-DOS � CP/M
5 � ������������� ������� CP/M
6 � ������� ������ ������ CP/M (dir)
7 � ������� ��� CP/M ������
8 � ������� ����� CP/M

���������� ������ �������������� �� ������� ����.
������� �������� �������� �� ����� 1 � ����� ���� ������� CP/M. ��� ���������� ������ ���������� ��������� ������� ��� ������������ ������� (�����, ������, ��������� ������), � �� ��������� ����� ����� 160 �����. 22Disk ����������� � �������� ��� WinXP.

> ����� ������ FX86  (http://ftp.gaby.de/pub/dos/fx86.zip)
����������� ��� MS-DOS, ���������� �� �������� � 22disk. ������������ ����� 6 ���������� ��������, ������ �� ������� ��������� ������������ �������.
Pcopy.exe � ��������� ���������� ����� � ��� �� ������� CP/M
Pdel.exe � ������� ����� � ������� CP/M
Pdir.exe � ���������� ���������� ������� CP/M
Pformat.exe � ��������� ��������������� ������� CP/M (720,360 kb)
Pset.exe � ��������� ���������� �������� ��� ������ �� ������� CP/M
Ptype.exe � ������������� ���������� CP/M �����

��� ���������� ��������� �������� ���������� ��������� ������ ������� � ����������� �� ��������� ������. ��� ��������� ��������� �������� ��� Windows XP, ���� � 3,5� ���������.

8. ��������� CP/M ��� MS-DOS � Windows.

- 22nice  (ftp://ftp.gts.cz/simtelnet/msdos/emulate/22nce142.zip)
���������� ����� Sydex. ������ 1.42, ���� 1994 ����. ��� ��������  8-������ ������� �� ���� CP/M 2.2, ������������ �������������� Intel 8080, 8085 ��� Zilog Z80. ��������� ������������� ��������� CP/M � ������ MS-DOS (com-����). ����� ���������� ���� ��������� ���������� ����. ������������ �����������. �������� ��� XP.

- MyZ80  (http://www.gaby.de/myz80.zip)
�������� ������� CP/M �� ���� Z80. ������ 1.24 Copyright 1991-1998 by Simeon Cran.
����� ������� ���������� ���������� � ������ � �������� ���������������� ��, ����� ������������ ������� ����������� �� ��������� � ������� CP/M. � ����� �� ����� �������� ��� ��������� ������. ������� ����� �� ������ ����. ����� ���������� � ������. ������� � MS-DOS �� ������� exit.  �������� ��� Windows XP.

-cpm86  (http://www.gaby.de/cpm86.exe)
�������� CP/M-86. ������ 1.3 �� 30.11.1997. � ��������� 1 ������������ ���� cpm86.exe. ��� ������� � ��������� ������ ����� ������� �������� � ��� ������������ ����� cp/m-86 (*.cmd). �������� ��� Windows XP. �������� ��������� ������ �������.

- ame86  (ftp://ftp.gts.cz/simtelnet/msdos/emulate/ame86.zip) 
Application Migration Executive CP/M-86 �������� ��� MS-DOS. Test-version 0.7.1.
Copyright by Jean-Mark Lugrin, Digital Equipment Corporation, 1984.
��� ������� ���������� ������� � ��������� ������ ��� ������������ *.cmd ����� CP/M-86.
��� ������� �������� ���������� ��� ���������� �����, ������� ��� ���������� ������� cd/dvd-����� � ���������� �������, ���� ������� �������. �������� ������ �� ������.

- Soviet PC. ( http://soviet-pc.narod.ru/ )
�� ��� ������ ������� �������. ��� �������� CP/M-86 ����� ����� ������� ������: http://ftp.gaby.de/pub/cpm/sysdisks/cpm86/86raw144.zip  (������ ������ �������� �� ���).
��������� ����� ��� ���� A: (����� ����: ��������� -> ���� A: -> ��������� �� �����)
�������� ��������� ��������� ��� ����������. ��� ���������� ������ � ������ 0.18.1 ��� �������� ��������� ��� ���������� Intel 80386 � ������������ CGA. Soviet PC � ������ �����������, ������� �������� ��������� �����������. �������� ������ ��� Windows.

-VMWare ()
�������� ����� ������ � ������ ������� ���������. ��� �������� CP/M-86 ����� ����� ����� �������: http://ftp.gaby.de/pub/cpm/sysdisks/cpm86/86raw144.zip
����������: 
���� �� ������ �������� � ����� ����������� � �� �������� ���������� ��� ������� ��������� � BIOS`� ����������� ������.
���� ���������� �������� � ���������, ���������� � ������� ������ ������ FX86, �� � BIOS`� ����������� ������ ����� ���������� ��� ���������: 5,25� 360 ��.

 9. ������ ����������.

    ����� ���� ������������ ������� ��������, ������������������� ������ � ������� ������������ ������������ ������� CP/M. �� ������ ���� �������. �������, ������� ��� �� ����� ����� �������. ���� �� ������ ���� ����������� ������ ������ �������� �� Digital Research, ���� �� �1980 ���� ��� �� ������� ��������� ������ � IBM, � ������ MS-DOS ����� ������������ ����������� �������� �� � CP/M. 
���������� �����������, ����������� �������, ���� ����� �����. ���� ���� � 1994 ���� � �������� 52 ���, ���, ��������, � �� ������� �� ����� ���� ���� ������. ���� ����, �� ��, � ��� ����, �������� ��������� � ����� �������.
� ��������� ��������� �� ���� MS-DOS � ����� ������� Microsoft ��� ��� ����� ������� ���� ��������, �� ������ ���, ����� �� ��������� Windows, ���� �� 98, XP ��� Vista �� ����� ����� � �������, ��� ���-�� ���, ������ ���� ����, ���� ���� �� ��������� �������� CP/M�

P.S. � ��� ���� CP/M ����! ���� � 21 ���� ��� ������� ����������. � ���� ����� ��������� IMSAI ����������� ��������� �IMSAI SERIES TWO� ���������� ��� ���� ������������ ��������! (http://www.imsai.net/products/imsai_series_two.htm)



