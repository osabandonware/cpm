REM  *---------------------------------------------------------------------*
REM  *                  FIX PC DOS DR DRAW FOR PC-MODE                     *
REM  *---------------------------------------------------------------------*
REM  *                                                                     *
REM  *       Make sure the FIX-IT disk is in the default drive             *
REM  *       and that the PC DOS DR DRAW disk is in drive "%1".            *
REM  *                                                                     *
REM  *       If the above is not correct:                                  *
REM  *          - Hold down CTRL key and press "C"                         *
REM  *          - Press "Y"                                                *
REM  *                                                                     *
REM  *       Otherwise . . .                                               *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.exe
copy ginstall.cmd %1/v
erase %1current.trm
copy current.trm  %1/v
erase %1gsx.exe
copy gsx.cmd %1/v
copy %1assign.sys /v
pip %1=assign.sys[v]
REM  *---------------------------------------------------------------------*
REM  *            DR DRAW is now ready to run under PC-MODE                *
REM  *---------------------------------------------------------------------*
