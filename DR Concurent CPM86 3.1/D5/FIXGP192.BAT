REM  *---------------------------------------------------------------------*
REM  *                  FIX PC DOS DR DRAW FOR PC-MODE                     *
REM  *---------------------------------------------------------------------*
REM  *                                                                     *
REM  *       Make sure the FIX-IT disk is in the default drive             *
REM  *       and that the PC DOS DR DRAW disk is in drive "%1".            *
REM  *                                                                     *
REM  *       If the above is not correct:                                  *
REM  *          - Hold down CTRL key and press "C"                         *
REM  *          - Press "Y"                                                *
REM  *                                                                     *
REM  *       Otherwise . . .                                               *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.exe
copy ginstall.cmd %1/v
erase %1current.trm
copy current.trm  %1/v
erase %1gsx.exe
copy gsx.cmd %1/v
copy %1assign.sys /v
pip %1=assign.sys[v]
REM  *---------------------------------------------------------------------*
REM  *            DR DRAW is now ready to run under PC-MODE                *
REM  *---------------------------------------------------------------------*

REM  *---------------------------------------------------------------------*
REM  *           FIX PC DOS DR GRAPH REQUIRING 128K FOR PC-MODE            *
REM  *---------------------------------------------------------------------*
REM  *                                                                     *
REM  *       Make sure the FIX-IT disk is in the the default drive         *
REM  *       and that the PC DOS DR GRAPH disk is in drive "%1".           *
REM  *                                                                     *
REM  *       If the above is not correct:                                  *
REM  *          - Hold down CTRL key and press "C"                         *
REM  *          - Press "Y"                                                *
REM  *                                                                     *
REM  *       Otherwise . . .                                               *
REM  *---------------------------------------------------------------------*
pause
erase %1gsx.exe
copy gsx.cmd %1/v
copy %1assign.sys /v
pip %1=assign.sys[v]
REM  *---------------------------------------------------------------------*
REM  *         Place DR GRAPH Driver Library I disk in drive "%1".         *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.exe
erase %1ginstall.cmd
copy ginstall.cmd %1/v
erase %1current.trm
copy current.trm %1/v
REM  *---------------------------------------------------------------------*
REM  *        Place DR GRAPH Driver Library II disk in drive "%1".         *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.dis
copy ginstall.dis %1/v
REM  *---------------------------------------------------------------------*
REM  *      DR GRAPH requiring 128K is now ready to run under PC-MODE.     *
REM  *---------------------------------------------------------------------*

REM  *---------------------------------------------------------------------*
REM  *             FIX PC DOS DR GRAPH REQUIRING 192K AND 8087             *
REM  *                            FOR PC-MODE                              *
REM  *---------------------------------------------------------------------*
REM  *                                                                     *
REM  *       Make sure the FIX-IT disk is in the the default drive         *
REM  *       and that the PC DOS DR GRAPH disk is in drive "%1".           *
REM  *                                                                     *
REM  *       If the above is not correct:                                  *
REM  *          - Hold down CTRL key and press "C"                         *
REM  *          - Press "Y"                                                *
REM  *                                                                     *
REM  *       Otherwise . . .                                               *
REM  *---------------------------------------------------------------------*
pause
erase %1gsx.exe
copy gsx.cmd %1/v
copy %1assign.sys /v
pip %1=assign.sys[v]
erase %1graph.bat
copy graph87.bat %1graph.bat/v
REM  *---------------------------------------------------------------------*
REM  *         Place DR GRAPH Driver Library I disk in drive "%1".         *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.exe
erase %1ginstall.cmd
copy ginstall.cmd  %1/v
erase %1current.trm
copy current.trm %1/v
REM  *---------------------------------------------------------------------*
REM  *        Place DR GRAPH Driver Library II disk in drive "%1".         *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.dis
copy ginstall.dis %1/v
REM  *---------------------------------------------------------------------*
REM  *            DR GRAPH requiring 192K and 8087 is now                  *
REM  *                  ready to run under PC-MODE.                        *
REM  *---------------------------------------------------------------------*

REM  *---------------------------------------------------------------------*
REM  *            FIX PC DOS DR GRAPH REQUIRING 192K FOR PC-MODE           *
REM  *---------------------------------------------------------------------*
REM  *                                                                     *
REM  *       Make sure the FIX-IT disk is in the the default drive         *
REM  *       and that the PC DOS DR GRAPH disk is in drive "%1".           *
REM  *                                                                     *
REM  *       If the above is not correct:                                  *
REM  *          - Hold down CTRL key and press "C"                         *
REM  *          - Press "Y"                                                *
REM  *                                                                     *
REM  *       Otherwise . . .                                               *
REM  *---------------------------------------------------------------------*
pause
erase %1gsx.exe
copy gsx.cmd %1/v
copy %1assign.sys /v
pip %1=assign.sys[v]
erase %1graph.bat
copy graph192.bat %1graph.bat/v
REM  *---------------------------------------------------------------------*
REM  *         Place DR GRAPH Driver Library I disk in drive "%1".         *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.exe
erase %1ginstall.cmd
copy ginstall.cmd %1/v
erase %1current.trm
copy current.trm %1/v
REM  *---------------------------------------------------------------------*
REM  *        Place DR GRAPH Driver Library II disk in drive "%1".         *
REM  *---------------------------------------------------------------------*
pause
erase %1ginstall.dis
copy ginstall.dis %1/v
REM  *---------------------------------------------------------------------*
REM  *      DR GRAPH requiring 192K is now ready to run under PC-MODE.     *
REM  *---------------------------------------------------------------------*
