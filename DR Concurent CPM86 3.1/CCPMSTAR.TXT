;;=========================================================
;; "Concurrent CP/M Operating System"
;; "For use on Starlink, IBM PC, PC XT, and"
;; "all 100% PC compatibles"
;;------------------------------------
;; PRODUCT NAME   : StarLink (tm)
;; IMPLEMENTATION : CCP/M PC-DOS
;; VERSION NUMBER : IBM PC or PC XT
;; SERIAL NUMBER  : 2004-0000-000268
;;------------------------------------
; Diskette image files list, order of recording session:
;	made with COPYQM. 
;
CCPMST5R.DSK <- "PC-DOS GSX Patch For DR Draw, DR Graph"
;		disk 5 of 5
;
CCPMST4R.DSK <- "Terminal Install Disk For IBM PC or PCXT"
;		disk 4 of 5
;
CCPMST3R.DSK <- "Utilities II" disk 3 of 5
;
CCPMST2R.DSK <- "Utilities I" disk 2 of 5
;****
CCPMST1R.BAD <- 2nd failed read attempt, recorded anyway
;		with 'include errors anyway' option. error
;		on cycl 34 side 1.
;****
CCPMSTAR.DSK <- 3rd attempt !read without err! PTL!
;
CCPMST1R.DSK <- 4th attempt !read w/o err!, used this to
;		make a backup disk. [see note1 below]
;
CCPMS1.DSK   <- 5th attempt !read w/o err! PTL!!
;		a backup to the backup, ***so these last 3
;		diskette images are identical***
;
Note1: This readable diskette loads CCP/M, however the OS
	initialization gives "Multi-user card not installed"
	if the StarLink CoProcessor Card isn't installed.
Note2: Disk 1 of 5 is labelled "Boot Diskette 1984"
	"Program Starlink(tm) Concurrent(tm) PC-DOS
	 Version For IBM(c) PC or PCXT
	 Serial# 2004-0000-000268"
;eof feb 1 2004;
